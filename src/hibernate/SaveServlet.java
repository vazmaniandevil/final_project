package hibernate;

import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

@WebServlet(name = "SaveServlet", value = "/SaveServlet")
public class SaveServlet extends HttpServlet {

    static String date;
    static String serial;
    static String description;


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try {
            ConnectDatabase t = ConnectDatabase.getInstance();
            List<Parts> c = t.showParts();

            response.setContentType("text/html");
            PrintWriter out = response.getWriter();
            out.println("<html><head>");
            out.println("<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css\">\n");
            out.println("<script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js\"></script>\n");
            out.println("</head><body>");
            out.println("<div class=\"container\">");
            out.println("<form action=\"index.jsp\">");
            out.println("<input class=\"btn btn-primary\" type=\"submit\" value=\"Go Back\"/>");
            out.println("</form>");
            out.println("<table class=\"table table-sm table-bordered table-hover\">");
            out.println("<th>ID</th>");
            out.println("<th>DATE</th>");
            out.println("<th>SERIAL</th>");
            out.println("<th>DESCRIPTION</th>");
            for (Parts i : c) {
                out.println("<tr>");
                out.println("<td>" + i.getId() + "</td>");
                out.println("<td>" + i.getDate() + "</td>");
                out.println("<td>" + i.getSerial() + "</td>");
                out.println("<td>" + i.getDescription() + "</td>");
                out.println("</tr>");
            }
            out.println("</table>");
            out.println("</div>");
            out.println("</body></html>");
        }
        catch (Exception e) {
            System.out.println("Could not process doGet method");
        }
    }

    /*@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try {
            ConnectDatabase t = ConnectDatabase.getInstance();
            List<Parts> c = t.showParts();

            //Instantiating the File class
            File file = new File("C:\\Users\\Wesley Vaz\\Desktop\\School\\Winter Semester 2021\\CIT 360 (Java Programing)\\Git\\final_project\\src\\main\\webapp\\sample.txt");
            //System.out.println("Printed to " + file.getAbsolutePath());
            //Instantiating the PrintStream class
            PrintStream stream = new PrintStream(file);
            System.setOut(stream);
            //Printing values to file
            for (Parts i : c) {
                System.out.println(i + "\n");
            }

            Scanner scanner = new Scanner(new File("C:\\Users\\Wesley Vaz\\Desktop\\School\\Winter Semester 2021\\CIT 360 (Java Programing)\\Git\\final_project\\src\\main\\webapp\\sample.txt"));

            response.setContentType("text/html");
            PrintWriter out = response.getWriter();
            out.println("<html><head></head><body>");
            out.println("<form action=\"index.jsp\">");
            out.println("<input type=\"submit\" value=\"Go Back\"/>");
            out.println("</form>");
            while (scanner.hasNextLine()) {
                out.println(scanner.nextLine() + "<br/>");
            }
            out.println("</body></html>");
        }
        catch (Exception e) {
            System.out.println("Could not process doGet method");
        }
    }*/

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try
        {
            PrintWriter out = response.getWriter();
            response.setContentType("text/html");
            out.println("<html><head></head><body>");
            date = request.getParameter("date");
            serial = request.getParameter("serial");
            description = request.getParameter("description");
            ConnectDatabase t = ConnectDatabase.getInstance();
            t.insertNewPart();
            out.println("<h1>Your parts have been added successfully!</h1>");
            out.println("<p>Date: " + date + "</p>");
            out.println("<p>Serial: " + serial + "</p>");
            out.println("<p>Description: " + description + "</p>");
            out.println("<form action=\"index.jsp\">");
            out.println("<input type=\"submit\" value=\"Go Back\" />");
            out.println("</form>");
            out.println("</body></html>");
        }
        catch (IOException e)
        {
            System.out.println("Could not process doPost method");
        }
    }

    public static List partsAdded() {

        //String[] newParts = {date, serial, description};

        List newParts = new ArrayList();
        newParts.add(date);
        newParts.add(serial);
        newParts.add(description);

        return newParts;
    }

}
