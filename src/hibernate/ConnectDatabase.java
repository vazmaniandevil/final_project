package hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import java.util.*;

public class ConnectDatabase {

    SessionFactory factory;
    Session session = null;

    private static ConnectDatabase single_instance = null;

    ConnectDatabase() { factory = HibernateUtils.getSessionFactory();
    }

    public static ConnectDatabase getInstance() {
        if (single_instance == null) {
            single_instance = new ConnectDatabase();
        }

        return single_instance;
    }

    public List<Parts> showParts() {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from hibernate.Parts";
            List<hibernate.Parts> list = (List<hibernate.Parts>)session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return list;

        } catch (Exception e) {
            System.out.println("Failed retrieving data");
            session.getTransaction().rollback();
            return null;
        } /*finally {
            session.close();
            hibernateweek.HibernateUtils.shutdown();
        }*/
    }

    public static void insertNewPart() {
        Session session = HibernateUtils.getSessionFactory().openSession();
        session.beginTransaction();

        try {
            String date;
            String serial;
            String description;

            List newPartsArray = SaveServlet.partsAdded();

            System.out.print("\n Date: ");
            date = (String) newPartsArray.get(0);

            System.out.print("\n Serial: ");
            serial = (String) newPartsArray.get(1);

            System.out.print("\n Description: ");
            description = (String) newPartsArray.get(2);

            Parts newParts = new Parts();
            newParts.setDate(date);
            newParts.setSerial(serial);
            newParts.setDescription(description);

            session.save(newParts);
            session.getTransaction().commit();

        } catch (Exception e) {
            System.out.println("New entry could not be added");
        }
    }
}


